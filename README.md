# README #


## Run test

Run all tests from current dir
```
npx codeceptjs run
```

Run single test [path to codecept.js] [test filename]
```
npx codeceptjs run github_test.js
```

See more: [Commands](https://codecept.io/commands/)

## Parallel Execution And Multiple Browsers Execution

### Parallel Execution 

To run tests in parallel:
```
npx codeceptjs run-workers --suites 2
```

### Multiple Browsers Execution

Run all suites for all browsers:
```
codeceptjs run-multiple --all
```

Run basic suite for all browsers:
```
codeceptjs run-multiple basic
```

See more: [codecept.io](https://codecept.io/parallel/#multiple-browsers-execution)

## Adding the Allure plugin

Install the Allure commandline used to generate and serve the report (currently only works when installed globally):
```
npm install -g allure-commandline --save-dev
```

Run the tests:
```
npx codeceptjs run --steps --plugins allure
```

Serve the Allure report:
```
allure serve output
```

See more: [Allure](https://codecept.io/plugins/#allure)

