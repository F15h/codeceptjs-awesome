const { setHeadlessWhen } = require('@codeceptjs/configure');

setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './tests/*_test.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'https://facebook.com',
      show: true,
      browser: 'chromium',
      windowSize: '1100x800',
    }
  },
  include: {
    I: './steps/custom_steps.js',
    searchBarFragment: './fragments/searchBar.js',
    testPagePage: './pages/testPage.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'codecept-playwright-facebook',
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    },
    allure: {
      enabled: true,
      outputDir: "./output"
    }
  },
  multiple: {
    basic: {
      browsers: ["chromium", "firefox", "webkit"]
    }
  }
}