const { searchBarFragment } = inject();

module.exports = function() {
    return actor({
        searchUseBar: function(text) {
            this.seeElement(searchBarFragment.searchField)
            this.fillField(searchBarFragment.searchField, text)
            this.pressKey('Enter')
        }
    });
}